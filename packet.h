/*
 * paket.h
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 */

#ifndef _PACKET_H
#define _PACKET_H

#include <stdbool.h>

#include "schnakebus.h"

bool packet_send(uint8_t address, struct buspacket_t *packet, bool wait);

#endif
