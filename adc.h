/*
 * adc.h
 *
 * Definition for the battery ADC conversion
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 *
 */

#ifndef ADC_H_
#define ADC_H_

#include <stdint.h>

extern void adc_init();
extern uint16_t adc_read();
extern void adc_off();

#endif /* ADC_H_ */
