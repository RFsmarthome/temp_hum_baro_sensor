/*
 * lt8900.cpp
 *
<<<<<<< HEAD
 * Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This version based on the library from Rob van der Veer.
 * The library was modified to support deep sleep an interrupted
 * base wake ups.
 * Registered are based on last datasheet recommendations.
 *
 * Send and listen could now be address based.
=======
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
>>>>>>> e1
 */

#include "lt8900.h"

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include "schnakebus.h"

#include "defines.h"

static uint8_t m_channel = 0;
static uint8_t m_retransmit;

inline void lt8900_waitSPI()
{
	while((SPSR & _BV(SPIF)) == 0);
}

void lt8900_setSelect(bool select)
{
	if(select) {
		LT8900_CS_PORT &= ~_BV(LT8900_CS_PIN);
	} else {
		LT8900_CS_PORT |= _BV(LT8900_CS_PIN);
	}
}

void lt8900_init(uint8_t retransmit, uint8_t channel, bool pkt_pullup)
{
	m_retransmit = retransmit;
	m_channel = channel;
	DDRB |= _BV(PB3) | _BV(PB5); // MOSI SCK as output

	LT8900_CS_DDR |= _BV(LT8900_CS_PIN); // CS as output
	lt8900_setSelect(false);
	LT8900_CS_PORT |= _BV(LT8900_CS_PIN); // and high

	SPCR = _BV(SPE) | _BV(MSTR) | _BV(CPHA); // Enable SPI, Master
	SPSR |= _BV(SPI2X);

	LT8900_RST_DDR |= _BV(LT8900_RST_PIN); // RST pin as output
	LT8900_RST_PORT |= _BV(LT8900_RST_PIN); // RST pin as output

	LT8900_PKT_DDR &= ~_BV(LT8900_PKT_PIN); // PKT pin as input
	if(pkt_pullup) {
		LT8900_PKT_PORT |= _BV(LT8900_PKT_PIN); // PKT Pull up on
	} else {
		LT8900_PKT_PORT &= ~_BV(LT8900_PKT_PIN); // PKT Pull up off
	}

	lt8900_reset();
}

void lt8900_reset()
{
	LT8900_RST_PORT &= ~_BV(LT8900_RST_PIN);
	_delay_ms(20);
	LT8900_RST_PORT |= _BV(LT8900_RST_PIN);
	_delay_ms(5);
}

bool lt8900_test()
{
	uint8_t data[3];

	data[0] = 0x80;
	data[1] = 0x00;
	data[2] = 0x00;

	lt8900_setSelect(true);
	_delay_us(250);

	SPDR = data[0];
	lt8900_waitSPI();
	data[0] = SPDR;
	_delay_us(50);

	SPDR = data[1];
	lt8900_waitSPI();
	data[1] = SPDR;
	_delay_us(50);

	SPDR = data[2];
	lt8900_waitSPI();
	data[2] = SPDR;

	_delay_us(250);
	lt8900_setSelect(false);

	if(data[0] == 0x01 && data[1] == 0x6f && data[2] == 0xe0) {
		return true;
	}
	return false;
}

uint16_t lt8900_readRegister(uint8_t reg)
{
	uint8_t data[3];

	data[0] = reg | 0x80;
	data[1] = 0x00;
	data[2] = 0x00;

	lt8900_setSelect(true);
	_delay_us(250);

	SPDR = data[0];
	lt8900_waitSPI();
	data[0] = SPDR;
	_delay_us(50);

	SPDR = data[1];
	lt8900_waitSPI();
	data[1] = SPDR;
	_delay_us(50);

	SPDR = data[2];
	lt8900_waitSPI();
	data[2] = SPDR;

	_delay_us(250);
	lt8900_setSelect(false);

	return (data[1] << 8 | data[2]);
}

uint16_t lt8900_writeRegister(uint8_t reg, uint16_t value)
{
	uint8_t data[3];

	data[0] = reg & 0x7f;
	data[1] = (uint8_t)(value >> 8);
	data[2] = (uint8_t)value & 0xff;

	lt8900_setSelect(true);
	_delay_us(250);

	SPDR = data[0];
	lt8900_waitSPI();
	data[0] = SPDR;
	_delay_us(50);

	SPDR = data[1];
	lt8900_waitSPI();
	data[1] = SPDR;
	_delay_us(50);

	SPDR = data[2];
	lt8900_waitSPI();
	data[2] = SPDR;

	_delay_us(250);
	lt8900_setSelect(false);

	return ((uint16_t)data[1]) << 8 | (uint16_t)data[2];
}

uint16_t lt8900_writeRegister2(uint8_t reg, uint8_t value1, uint8_t value2)
{
	uint8_t data[3];

	data[0] = reg & 0x7f;
	data[1] = value1;
	data[2] = value2;

	lt8900_setSelect(true);
	_delay_us(250);

	SPDR = data[0];
	lt8900_waitSPI();
	data[0] = SPDR;
	_delay_us(50);

	SPDR = data[1];
	lt8900_waitSPI();
	data[1] = SPDR;
	_delay_us(50);

	SPDR = data[2];
	lt8900_waitSPI();
	data[2] = SPDR;

	_delay_us(250);
	lt8900_setSelect(false);

	return ((uint16_t)data[1]) << 8 | (uint16_t)data[2];
}

void lt8900_setSyncWord(uint64_t syncword)
{
	lt8900_writeRegister(36, syncword & 0xffff);
	lt8900_writeRegister(37, (syncword >> 16) & 0xffff);
	lt8900_writeRegister(38, (syncword >> 32) & 0xffff);
	lt8900_writeRegister(39, (syncword >> 48) & 0xffff);
}

void lt8900_setAddress(uint8_t address)
{
	uint64_t sync = SYNC_WORD;
	sync ^= ((uint64_t)address << 56 | (uint64_t)address << 48 | (uint64_t)address << 40 | (uint64_t)address << 32
	        | (uint64_t)address << 24 | (uint64_t)address << 16 | (uint64_t)address << 8 | (uint64_t)address);
	lt8900_setSyncWord(sync);
	_delay_us(20);
}

void lt8900_setChannel(uint8_t channel)
{
	m_channel = channel;
	lt8900_writeRegister(7, m_channel & 0x007f);
}

uint8_t lt8900_getChannel()
{
	return m_channel;
}

void lt8900_init_hardware()
{
	// Set to recommended values
	/*
	 writeRegister(0, 0x6fef);
	 writeRegister(1, 0x5681);
	 writeRegister(2, 0x6617);
	 writeRegister(4, 0x9cc9);
	 writeRegister(5, 0x6637);
	 writeRegister(7, 0x0030);
	 writeRegister(8, 0x6c90);
	 writeRegister(9, 0x1840);
	 writeRegister(10, 0x7ffd);
	 writeRegister(11, 0x0008);
	 writeRegister(12, 0x0000);
	 writeRegister(13, 0x48bd);
	 writeRegister(22, 0x00ff);
	 writeRegister(23, 0x8005);
	 writeRegister(24, 0x0067);
	 writeRegister(25, 0x1659);
	 writeRegister(26, 0x19e0);
	 writeRegister(27, 0x1200);
	 writeRegister(28, 0x1800);
	 writeRegister(32, 0x1806 | 0b11010000); // FEC 1/3 Interleave
	 writeRegister(33, 0x3ff0);
	 writeRegister(34, 0x3000);
	 writeRegister(35, 0x0080 | ((m_retransmit & 0x0f)<<8));  // times re-transmit (4bit)
	 writeRegister(40, 0x2107);
	 writeRegister(41, 0b1011100000000000) ; // CRC & AUTO_ACK
	 writeRegister(42, 0xfdb0);
	 writeRegister(43, 0x000f);
	 */
	lt8900_writeRegister(0, 0x6fe0);
	lt8900_writeRegister(1, 0x5681);
	lt8900_writeRegister(2, 0x6617);
	lt8900_writeRegister(4, 0x9cc9);    //why does this differ from powerup (5447)
	lt8900_writeRegister(5, 0x6637);    //why does this differ from powerup (f000)
	lt8900_writeRegister(8, 0x6c90);    //power (default 71af) UNDOCUMENTED

	lt8900_writeRegister(9, 0x1840);
	lt8900_writeRegister(10, 0x7ffd);   //bit 0: XTAL OSC enable
	lt8900_writeRegister(11, 0x0008);   //bit 8: Power down RSSI (0=  RSSI operates normal)
	lt8900_writeRegister(12, 0x0000);
	lt8900_writeRegister(13, 0x48bd);   //(default 4855)

	lt8900_writeRegister(22, 0x00ff);
	lt8900_writeRegister(23, 0x8005);  //bit 2: Calibrate VCO before each Rx/Tx enable
	lt8900_writeRegister(24, 0x0067);
	lt8900_writeRegister(25, 0x1659);
	lt8900_writeRegister(26, 0x19e0);
	lt8900_writeRegister(27, 0x1300);  //bits 5:0, Crystal Frequency adjust
	lt8900_writeRegister(28, 0x1800);

	//fedcba9876543210
	lt8900_writeRegister(32, 0b1001100010010000); //AAABBCCCDDEEFFFG  A preamble length, B, syncword length, c trailer length, d packet type
	//                  E FEC_type, F BRCLK_SEL, G reserved
	//0x5000 = 0101 0000 0000 0000 = preamble 010 (3 bytes), B 10 (48 bits)
	lt8900_writeRegister(33, 0x3fc7);
	lt8900_writeRegister(34, 0x2000);  //
	lt8900_writeRegister(35, m_retransmit << 8);  //POWER mode,  bit 8/9 on = retransmit = 3x (default)

	//lt8900_setSyncWord(SYNC_WORD);

	lt8900_writeRegister(40, 0x4401);  //max allowed error bits = 0 (01 = 0 error bits)
	lt8900_writeRegister(41, 0x8000 | 0x4000 | 0x2000 | 0x1000 | (1 << 11));

	lt8900_writeRegister(42, 0xfdb0);
	lt8900_writeRegister(43, 0x000f);

	//lt8900_setCurrentControl(15, 0);

	lt8900_writeRegister(50, 0x0000);  //TXRX_FIFO_REG (FIFO queue)

	lt8900_writeRegister(52, 0x8080); //Fifo Rx/Tx queue reset

	_delay_us(200);
	lt8900_writeRegister(7, _BV(8));  //set TX mode.  (TX = bit 8, RX = bit 7, so RX would be 0x0080)
	_delay_us(2);
	lt8900_writeRegister(7, m_channel);  // Frequency = 2402 + channel
}

void lt8900_sleep()
{
	lt8900_writeRegister(7, m_channel);
	lt8900_writeRegister(35, (1 << 14));
}

/*
 void lt8900_powerdown()
 {
 lt8900_writeRegister(35, lt8900_readRegister(35) | (1<<15));
 RST_PORT &= ~_BV(RST_PIN);
 }
 */

void lt8900_wakeup()
{
	lt8900_setSelect(true);
	_delay_us(50);
	lt8900_setSelect(false);
	_delay_us(50);

	lt8900_writeRegister(35, m_retransmit << 8);  //POWER mode,  bit 8/9 on = retransmit = 3x (default)
	lt8900_writeRegister(7, m_channel);
}

void lt8900_setPower(uint8_t power, uint8_t gain)
{
	lt8900_writeRegister(9, ((power & 0x0f) << 12) | ((gain & 0x0f) << 7));
}

bool lt8900_waitPackage(uint32_t timeout)
{
	for(uint16_t t = 0; t < timeout * 5; ++t) {
		_delay_us(200);
		if((LT8900_PKT_PORT & LT8900_PKT_PIN) != 0) {
			return true;
		}
	}
	return false;
}

void lt8900_scanRSSI(uint16_t *buffer, uint8_t startChannel, uint8_t numChannels)
{
	lt8900_writeRegister(52, 0x8080);  //flush rx

	//set number of channels to scan.
	lt8900_writeRegister(42, (lt8900_readRegister(42) & 0b0000001111111111) | (((numChannels - 1) & 0b111111) << 10));

	//set channel scan offset.
	lt8900_writeRegister(43, (lt8900_readRegister(43) & 0b0000000011111111) | ((startChannel & 0b1111111) << 8));

	// start rssi scan
	lt8900_writeRegister(43, (lt8900_readRegister(43) & 0b0111111111111111) | (1 << 15));

	lt8900_waitPackage(15);

	uint8_t pos = 0;
	while(pos < numChannels) {
		uint16_t data = lt8900_readRegister(50);
		buffer[pos++] = data >> 8;
	}
}

uint8_t lt8900_getRSSI()
{
	return lt8900_readRegister(6) >> 10;
}

bool lt8900_hasData()
{
	if((LT8900_PKT_SPIN & _BV(LT8900_PKT_PIN)) != 0) {
		return true;
	} else {
		return false;
	}
}

void lt8900_listen(uint8_t address)
{
	lt8900_setAddress(address);
	lt8900_writeRegister(7, m_channel & 0x7f); // Set channel and clear rx and tx
	_delay_ms(3);
	lt8900_writeRegister(52, 0x8080); // Clear fifo bit
	lt8900_writeRegister(7, (m_channel & 0x7f) | (1 << 7)); // Enable RX bit
}

int lt8900_receive(void *buffer, int maxBufferSize)
{
	uint16_t status = lt8900_readRegister(48);
	// Check CRC error
	if((status & 0x8000) == 0) {
		uint16_t data = lt8900_readRegister(50);
		uint8_t length = data >> 8;
		if(length > maxBufferSize) {
			// Buffer to small
			return -2;
		}

		uint8_t pos = 0;
		((uint8_t*)buffer)[pos++] = data & 0xff;
		while(pos < length) {
			data = lt8900_readRegister(50);
			((uint8_t*)buffer)[pos++] = data >> 8;
			((uint8_t*)buffer)[pos++] = data & 0xff;
		}

		return length;
	} else {
		// CRC error
		return -1;
	}
}

bool lt8900_send(uint8_t address, const void *buffer, int length, bool wait)
{
	if(length < 1 || length > 255) {
		return false;
	}

	lt8900_setAddress(address);

	lt8900_writeRegister(7, 0);
	lt8900_writeRegister(52, 0x8000);

	uint8_t pos = 0;
	uint8_t msb, lsb;
	lt8900_writeRegister2(50, (uint8_t)length, ((uint8_t*)buffer)[pos++]);
	while(pos < length) {
		msb = ((uint8_t*)buffer)[pos++];
		lsb = ((uint8_t*)buffer)[pos++];
		lt8900_writeRegister2(50, msb, lsb);
	}

	lt8900_writeRegister(7, (m_channel & 0x7f) | (1 << 8));
	if(wait) {
		while((LT8900_PKT_SPIN & _BV(LT8900_PKT_PIN)) == 0);
	}
	return true;
}

bool lt8900_waitData(uint32_t timeout)
{
	if(timeout == 0) { // Compare and branch cause more sleep possible
		while(!lt8900_hasData())
			;
		return true;
	} else {
		unsigned int count = 0; // Timeout wait for data

		while(!lt8900_hasData() && count++ < timeout) {
			_delay_ms(1);
		}
		if(count >= timeout) {
			return false;
		}
		return true;
	}
}

extern void lt8900_clearFifo()
{
	lt8900_writeRegister(52, 0x8080); //Fifo Rx/Tx queue reset
}
