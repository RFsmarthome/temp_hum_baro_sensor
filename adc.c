/*
 * adc.cpp
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 *
 */

#include <stdint.h>

#include "adc.h"

#include <avr/io.h>
#include <avr/power.h>

void adc_init()
{
	ADCSRA |= _BV(ADEN); // Enable ADC

	power_adc_enable();

	ADMUX = _BV(REFS0); // VCC REF ADC0
	ADCSRA = _BV(ADPS2) | _BV(ADPS1); // Divider 64

	ADCSRA |= _BV(ADEN);

	DIDR0 = _BV(ADC0_BIT); // Disable digital buffer on ADC0

	ADCSRA |= _BV(ADSC);
	while(ADCSRA & _BV(ADSC));
	(void)ADCW;
}

uint16_t adc_read()
{
	ADCSRA |= _BV(ADSC);
	while(ADCSRA & _BV(ADSC));

	return ADCW;
}

void adc_off()
{
	ADCSRA &= ~_BV(ADEN);
	power_adc_disable();
}
