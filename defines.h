/*
 * defines.h
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 */

#ifndef DEFINES_H_
#define DEFINES_H_


/*
 * LT89000
 */
#define LT8900_CS_DDR DDRB
#define LT8900_CS_PORT PORTB
#define LT8900_CS_PIN PB2

#define LT8900_RST_DDR DDRC
#define LT8900_RST_PORT PORTC
#define LT8900_RST_PIN PC2

/* for INT0
#define PKT_DDR DDRD
#define PKT_PORT PORTD
#define PKT_SPIN PIND
#define PKT_PIN PD2
*/

#define LT8900_PKT_DDR DDRC
#define LT8900_PKT_PORT PORTC
#define LT8900_PKT_SPIN PINC
#define LT8900_PKT_PIN PC3



/*
 * DHT22
 */
#define DDR_SENSOR   DDRD
#define PORT_SENSOR  PORTD
#define PIN_SENSOR   PIND
#define SENSOR       PD7



/*
 * Debug LED
 */


#define LED_DDR DDRD
#define LED_PORT PORTD
#define LED_PIN PIND
#define LED_BIT PD6



#endif /* DEFINES_H_ */
