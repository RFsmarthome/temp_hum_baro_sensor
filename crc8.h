/*
 * Crc8.h
 *
 *  Created on: 02.11.2015
 *      Author: schnake
 */

#ifndef _CRC8_H_
#define _CRC8_H_

#include <stdint.h>

#define CRC_POLYNOM 0x5d

extern uint8_t crc_message(uint8_t polynom, const uint8_t *msg, uint8_t len);

#endif /* CRC8_H_ */
