/**
 * BMP085 and BMP180
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 */


#ifndef BMP085_H_
#define BMP085_H_

#include <stdio.h>
#include <avr/io.h>

#define BMP085_ADDR (0x77<<1) //0x77 default I2C address

#define BMP085_I2CFLEURYPATH "i2cmaster.h" //define the path to i2c fleury lib

extern void bmp085_init(uint8_t o);
extern void bmp085_setOversampling(uint8_t o);
extern int16_t bmp085_getTemperature(uint16_t ut);
extern int32_t bmp085_getPressure(uint32_t up);
extern uint16_t bmp085_readUt();
extern uint32_t bmp085_readUp();

#endif
