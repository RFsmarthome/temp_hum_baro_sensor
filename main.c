/*
 * main.c
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 */

#include <stdint.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include <avr/eeprom.h>

#include "schnakebus.h"

#include "lt8900.h"
#include "bmp085.h"
#include "am2302.h"
#include "wdt.h"
#include "adc.h"
#include "defines.h"
#include "packet.h"

#ifndef ADDRESS
uint8_t rf_address EEMEM = 2;
#else
uint8_t rf_address EEMEM = ADDRESS;
#endif

/* For board 1 */
//float vcc_ref EEMEM = 3260;

/* For board 2 */
#ifndef VCC_REF
float vcc_ref EEMEM = 3351;
#else
float vcc_ref EEMEM = VCC_REF;
#endif

int __attribute__((OS_main)) main()
{
	cli();

	uint8_t delay_count = 0;

	DDRB = 0x00;
	PORTB = 0xff;
	DDRC = 0x00;
	PORTC = 0xff;
	DDRD = 0x00;
	PORTD = 0xff;

	ACSR = 0x80;
	ADCSRA = 0x00;

	power_timer0_disable();
	power_timer1_disable();
	power_timer2_disable();
	power_usart0_disable();

	power_twi_enable();
	power_spi_enable();

	lt8900_init(7, 120, false);
	lt8900_init_hardware();
	lt8900_setPower(15, 7);
	lt8900_sleep();

	am2302_init();

	bmp085_init(3);

	// Wait for all sensor up
	_delay_ms(2000);

	sei();

	for(;;) {
		// delay sending every hour
		++delay_count;
		if(delay_count>=60) {
			delay_count = 0;
			_delay_loop_2(rand());
		}

		// prepare rf packet
		struct buspacket_t packet;
		packet.version = SB_VERSION;
		//packet.type = WEATHER_STATION;
		packet.source = eeprom_read_byte(&rf_address);

		// Wakeup from sleep
		lt8900_wakeup();
		wdt_powerdown(WDT_05SEC);

		// Read sensor datas
		// BMP180
		int16_t bmp_temp = bmp085_getTemperature(bmp085_readUt());
		int32_t bmp_pres = bmp085_getPressure(bmp085_readUp());
		packet.type = TYPE_BMP180;
		packet.bmp180.pressure = bmp_pres;
		packet.bmp180.temperature = bmp_temp;
		packet_send(ADDR_MASTER, &packet, true);

		// DHT22
		int16_t dht_temp;
		uint16_t dht_hum;
		packet.type = TYPE_DHT22;
		if(am2302_read(&dht_hum, &dht_temp)==0) {
			packet.dht22.humanity = dht_hum;
			packet.dht22.temperature = dht_temp;
		} else {
			packet.dht22.humanity = -1;
			packet.dht22.temperature = 0;
		}
		packet_send(ADDR_MASTER, &packet, true);


		// Battery via ADC
		adc_init();
		_delay_ms(1);
		adc_read(); // Dummy read
		_delay_ms(1);
		adc_off();

		uint32_t sum = 0;
		for(uint8_t a=0; a<8; ++a) {
			sum += adc_read();
			_delay_us(10); // Delay for charge capacitor
		}
		adc_off();
		sum /= 8;
		uint16_t v = (eeprom_read_float(&vcc_ref) * sum / 1024.0);
		packet.type = TYPE_BATTERY;
		packet.battery.battery = v;
		packet.battery.battery_raw = sum;
		packet_send(ADDR_MASTER, &packet, true);



		// Sleep lt8900
		lt8900_sleep();

		// Random wait for async transmitting
		switch(packet.crc8 & 0b11) {
		case 0:
			wdt_powerdown(WDT_1SEC);
			break;
		case 1:
			wdt_powerdown(WDT_2SEC);
			break;
		case 2:
			wdt_powerdown(WDT_4SEC);
			break;
		case 3:
			wdt_powerdown(WDT_8SEC);
			break;
		}

		/* Power down for ~60secs */
		wdt_powerdown(WDT_8SEC);
		wdt_powerdown(WDT_8SEC);
		wdt_powerdown(WDT_8SEC);
		wdt_powerdown(WDT_8SEC);
		wdt_powerdown(WDT_8SEC);
		wdt_powerdown(WDT_8SEC);
		wdt_powerdown(WDT_8SEC);
		wdt_powerdown(WDT_4SEC);

		MCUSR = 0;
		wdt_disable();
	}
}
