/*
bmp085 lib 0x01

copyright (c) Davide Gironi, 2012

Released under GPLv3.
Please refer to LICENSE file for licensing information.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <avr/io.h>
#include <util/delay.h>

#include "bmp085.h"
#include "i2cmaster.h"

static uint8_t oss;
static int16_t ac1;
static int16_t ac2;
static int16_t ac3;
static uint16_t ac4;
static uint16_t ac5;
static uint16_t ac6;
static int16_t b1;
static int16_t b2;
static int16_t mb;
static int16_t mc;
static int16_t md;
static int32_t b5;
// static int16_t t;
// static int32_t p;

long bmp085_rawpressure;

/*
 * i2c write
 */
void bmp085_writeMem(uint8_t reg, uint8_t value)
{
	i2c_start_wait(BMP085_ADDR | I2C_WRITE);
	i2c_write(reg);
	i2c_write(value);
	i2c_stop();
}

/*
 * i2c read
 */
void bmp085_readMem(uint8_t reg, uint8_t buff[], uint8_t bytes)
{
	uint8_t i =0;
	i2c_start_wait(BMP085_ADDR | I2C_WRITE);
	i2c_write(reg);
	i2c_rep_start(BMP085_ADDR | I2C_READ);
	for(i=0; i<bytes; i++) {
		if(i==bytes-1)
			buff[i] = i2c_readNak();
		else
			buff[i] = i2c_readAck();
	}
	i2c_stop();
}

/*
 * Set oversampling for library
 */
void bmp085_setOversampling(uint8_t o)
{
	oss = o;
}

int16_t bmp085_readInt(uint8_t address)
{
	uint8_t buff[2] = {0,0};

	bmp085_readMem(address, buff, 2);
	return((int16_t)buff[0]<<8 | buff[1]);
}


/*
 * read calibration registers
 */
void bmp085_getCalibration()
{
	ac1 = bmp085_readInt(0xAA);
	ac2 = bmp085_readInt(0xAC);
	ac3 = bmp085_readInt(0xAE);
	ac4 = bmp085_readInt(0xB0);
	ac5 = bmp085_readInt(0xB2);
	ac6 = bmp085_readInt(0xB4);
	b1 = bmp085_readInt(0xB6);
	b2 = bmp085_readInt(0xB8);
	mb = bmp085_readInt(0xBA);
	mc = bmp085_readInt(0xBC);
	md = bmp085_readInt(0xBE);
}

uint16_t bmp085_readUt()
{
	// Write 0x2E into Register 0xF4
	// This requests a temperature reading
	bmp085_writeMem(0xf4, 0x2e);

	// Wait at least 4.5ms
	_delay_ms(5);

	// Read two bytes from registers 0xF6 and 0xF7
	return bmp085_readInt(0xf6);
}


uint32_t bmp085_readUp()
{
	unsigned long up = 0;
	uint8_t buffer[3];

	// Request a pressure reading w/ oversampling setting
	bmp085_writeMem(0xf4, 0x34 | (oss << 6));

	// Wait for conversion, delay time dependent on OSS set to max
	_delay_ms(2 + (3 << 3));

	// Read register 0xF6 (MSB), 0xF7 (LSB), and 0xF8 (XLSB)
	bmp085_readMem(0xf6, buffer, 3);

	up = (((uint32_t) buffer[0] << 16) | ((uint32_t) buffer[1] << 8) | (uint32_t) buffer[2]) >> (8-oss);
	return up;
}

int16_t bmp085_getTemperature(uint16_t ut)
{
	int32_t x1, x2;
	x1 = (((int32_t)ut - (int32_t)ac6) * (int32_t)ac5) >> 15;
	x2 = ((int32_t)mc << 11) / (x1 + md);
	b5 = x1 + x2;

	return ((b5 + 8)>>4);
}

int32_t bmp085_getPressure(uint32_t up)
{
	int32_t x1, x2, x3, b3, b6, p;
	uint32_t b4, b7;

	b6 = b5 - 4000;
	// Calculate B3
	x1 = (b2 * (b6 * b6)>>12) >> 11;
	x2 = (ac2 * b6) >> 11;
	x3 = x1 + x2;
	b3 = (((((int32_t)ac1) * 4 + x3) << oss) + 2) >> 2;

	// Calculate B4
	x1 = (ac3 * b6) >> 13;
	x2 = (b1 * ((b6 * b6) >> 12)) >> 16;
	x3 = ((x1 + x2) + 2) >> 2;
	b4 = (ac4 * (uint32_t)(x3 + 32768)) >> 15;

	b7 = ((uint32_t)(up - b3) * (50000 >> oss));
	if (b7 < 0x80000000) {
		p = (b7 << 1) / b4;
	} else {
		p = (b7 / b4) << 1;
	}
	x1 = (p >> 8) * (p >> 8);
	x1 = (x1 * 3038) >> 16;
	x2 = (-7357 * p) >> 16;
	p += (x1 + x2 + 3791) >> 4;

	return p;
}

/*
 * init bmp085
 */
void bmp085_init(uint8_t o)
{
	oss = o;

	i2c_init();
	_delay_us(10);

	bmp085_getCalibration(); //get calibration data
	bmp085_readUt();
}
